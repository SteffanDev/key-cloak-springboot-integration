package com.steffan.springboot.keycloak.proxies;

import com.steffan.springboot.keycloak.config.ClientConfiguration;
import com.steffan.springboot.keycloak.models.Provider;
import org.keycloak.adapters.springsecurity.client.KeycloakRestTemplate;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient( name = "providers-service", url= "${microservices.providers.url}" , configuration= {ClientConfiguration.class})
public interface ProviderProxy {


    @GetMapping("/providers/{id}")
    public Provider getProviderById( @PathVariable ("id") String id);
}

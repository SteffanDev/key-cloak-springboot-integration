package com.steffan.springboot.keycloak;

import com.steffan.springboot.keycloak.models.Provider;
import com.steffan.springboot.keycloak.proxies.ProviderProxy;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableAutoConfiguration
@EnableFeignClients("com.steffan.springboot.keycloak.proxies")

public class ProductApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProductApplication.class, args);
	}

}

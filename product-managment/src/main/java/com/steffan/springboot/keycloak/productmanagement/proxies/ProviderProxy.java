package com.steffan.springboot.keycloak.productmanagement.proxies;

import com.steffan.springboot.keycloak.productmanagement.config.ClientConfiguration;
import com.steffan.springboot.keycloak.productmanagement.models.Provider;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient( name = "providers-service", url= "${microservices.providers.url}" , configuration= {ClientConfiguration.class})
public interface ProviderProxy {


    @GetMapping("/providers/{id}")
    public Provider getProviderById(@PathVariable("id") String id);
}

package com.steffan.springboot.keycloak.controllers;

import com.steffan.springboot.keycloak.models.Product;
import com.steffan.springboot.keycloak.models.Provider;
import com.steffan.springboot.keycloak.proxies.ProviderProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@RequestMapping("/products")
public class ProductController {

    @Autowired
    ProviderProxy providerProxy;

    private List<Product> products = Stream.of(
            new Product("1","product_1", "1"),
            new Product("2","product_2","1"),
            new Product("3","product_3","2")
    ).collect(Collectors.toList());

    @GetMapping
    //@PreAuthorize("hasRole('user')")
    public List<Product> getAllProducts(){
        return products;
    }

    @GetMapping("/{id}")
    //@PreAuthorize("hasRole('ROLE_USER')")
    public ResponseEntity<Product> getProductDetails(@PathVariable String id){

        Optional<Product> product$ = products.stream().filter(p->id.equals(p.getId())).findFirst();
        if(!product$.isPresent()){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        Product product = product$.get();
        System.out.println(product.getId());
        Provider provider =  providerProxy.getProviderById(product.getId());
        product.setProvider(provider);
        return new ResponseEntity<Product>(product, HttpStatus.ACCEPTED);
    }

}

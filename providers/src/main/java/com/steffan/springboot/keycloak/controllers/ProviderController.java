package com.steffan.springboot.keycloak.controllers;


import com.steffan.springboot.keycloak.models.Provider;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.HttpEntityMethodProcessor;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@RequestMapping("/providers")
public class ProviderController {

    private List<Provider> providers = Stream.of(
            new Provider("1","Provider 1"),
            new Provider("2","Provider 2")
    ).collect(Collectors.toList());

    @GetMapping(value = "/{id}")
    public ResponseEntity<Provider> getProviderById(@PathVariable("id") String id ){
        Optional<Provider> provider = providers.stream().filter(p-> id.equals(p.getId())).findFirst();

        if(!provider.isPresent()){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        else {
            return new ResponseEntity<Provider>(provider.get(),HttpStatus.ACCEPTED);
        }
    }
}
